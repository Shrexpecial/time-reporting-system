package com.time.reporting.system.utils;

import javax.servlet.http.HttpSession;

public final class SessionHelper {


  /**
   * Checks if user is logged in the system.
   **/

  public static boolean isUserLogged (HttpSession httpSession) {
    return httpSession != null && httpSession.getAttribute("id") != null;
  }


  /**
   * Checks if admin is logged in the system.
   **/

  public static boolean isAdminLogged (HttpSession httpSession) {
    return httpSession != null && httpSession.getAttribute("id") != null &&
      httpSession.getAttribute("role").toString().equals("Admin");
  }
}
