package com.time.reporting.system.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.time.reporting.system.model.Log;
import com.time.reporting.system.model.Task;
import com.time.reporting.system.model.User;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.List;

/**
 * A class that holds the information for a specific report
 */
public class ReportsHelper implements Serializable {
     //A list of all the logs present in the database
    private List<Log> logs;
    //HashMap containing total time per task for each user
    private LinkedHashMap<User, LinkedHashMap<Task, LogHelper>> aggregateTime;


    public ReportsHelper() {
        this.logs = new ArrayList<Log>();
        this.aggregateTime = new LinkedHashMap<>();
    }

    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    /**
     * Adds time for a specific user to a specific task.
     * @param user
     * @param task
     * @param time - time in seconds
     */
    public void addTime(User user, Task task, Long time){
            if (this.aggregateTime.containsKey(user)){
                this.aggregateTime.get(user).put(task, new LogHelper(time));
            }
            else {
                LinkedHashMap<Task, LogHelper> temp = new LinkedHashMap<>();
                temp.put(task, new LogHelper(time));
                this.aggregateTime.put(user, temp);
            }
    }

    public LinkedHashMap<User, LinkedHashMap<Task, LogHelper>> getAggregateTime() {
        return aggregateTime;
    }

    public void setAggregateTime(LinkedHashMap<User, LinkedHashMap<Task, LogHelper>> aggregateTime) {
        this.aggregateTime = aggregateTime;
    }

    /**
     * Generates a pdf report
     * @throws Exception
     */
    public void createPDFReport(String path) throws Exception{
        File reportsDir = new File(path, "reports");
        if(!reportsDir.exists()) Files.createDirectories(reportsDir.toPath());

        Document document = new Document(PageSize.A4, 36, 36, 80, 60);
        PdfWriter writer = PdfWriter
                .getInstance(document, new FileOutputStream(reportsDir.getAbsolutePath()
                        + File.separator + "Report.pdf"));

        PageHelper event = new PageHelper();
        writer.setPageEvent(event);

        document.open();

        var titleFont = FontFactory.getFont(FontFactory.HELVETICA, 16, BaseColor.BLACK);
        var font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, BaseColor.BLACK);
        Paragraph title = new Paragraph("Report from Time Reporting System", titleFont);
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
        document.add(Chunk.NEWLINE);

        Paragraph detailedLog = new Paragraph("Detailed log:", font);
        document.add(detailedLog);

        var list = new com.itextpdf.text.List(com.itextpdf.text.List.ORDERED);
        for (Log log : this.getLogs()) {
            list.add(log.printLog() + System.lineSeparator());
        }
        document.add(list);

        Paragraph timePerUser = new Paragraph("Total time for each user by task:", font);
        document.add(Chunk.NEWLINE);
        document.add(timePerUser);

        this.getAggregateTime().forEach((user, taskLogs) -> {
                Paragraph userSpent = new Paragraph(user.getUsername() +
                        " spent:");
            try {
                document.add(userSpent);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
            com.itextpdf.text.List aggr = new com.itextpdf.text.List();
            taskLogs.forEach((task, loggedTime) -> {
                if(task.getName() != null){
                    aggr.add(task.getName() + " : Severity [" + task.getSeverity().toString() + "] " + " -> "
                            + loggedTime.print());
                }else {
                    aggr.add("Time spent in total" + " -> " + loggedTime.print());
                }
            });
            try {
                document.add(aggr);
            } catch (DocumentException e) {
                e.printStackTrace();
            }

        });
        document.close();
    }

}
