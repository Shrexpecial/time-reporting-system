package com.time.reporting.system.utils;

import java.io.Serializable;
import java.util.List;

public class AssignTaskHelper implements Serializable {

  private List<Long> usersID;
  private Long taskId;

  /**
   * Helper class to wrap the passed JSON.
   * JSON attributes:
   *  -List of user ids
   *  -Task id
   * */
  public AssignTaskHelper(){}

  public AssignTaskHelper (List<Long> usersID, Long taskId) {
    this.usersID = usersID;
    this.taskId = taskId;
  }

  public List<Long> getUsersID () {
    return usersID;
  }

  public void setUsersID (List<Long> usersID) {
    this.usersID = usersID;
  }

  public Long getTaskId () {
    return taskId;
  }

  public void setTaskId (Long taskId) {
    this.taskId = taskId;
  }
}
