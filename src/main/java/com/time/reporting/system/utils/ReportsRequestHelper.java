package com.time.reporting.system.utils;

import java.io.Serializable;
import java.util.List;

public class ReportsRequestHelper implements Serializable  {
    private List<Long> userIDs;
    private List<Long> taskIDs;

    public ReportsRequestHelper() {}

    public ReportsRequestHelper(List<Long> userIDs, List<Long> taskIDs) {
        this.userIDs = userIDs;
        this.taskIDs = taskIDs;
    }

    public List<Long> getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(List<Long> userIDs) {
        this.userIDs = userIDs;
    }

    public List<Long> getTaskIDs() {
        return taskIDs;
    }

    public void setTaskIDs(List<Long> taskIDs) {
        this.taskIDs = taskIDs;
    }
}
