package com.time.reporting.system.utils;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class LogHelper implements Serializable {

  private Long taskId ;
  private Long seconds;
  private Long minutes;
  private Long hours;
  private Long days;

  public LogHelper() {}

    /**
     * A constructor that converts seconds to days, hours, minutes, seconds.
     * @param time_spent - time spent in seconds.
     */
  public LogHelper(Long time_spent){
    //Get days worked
    days = time_spent/28800;
    //Get hours worked

    time_spent %= 28800;
    hours = TimeUnit.SECONDS.toHours(time_spent);

    //Get minutes worked

    time_spent %= 3600;
    minutes = TimeUnit.SECONDS.toMinutes(time_spent);

    //Get seconds worked
    seconds = time_spent%60;
  }

  public LogHelper (Long taskId, Long seconds, Long minutes, Long hours, Long days) {
    this.taskId = taskId;
    this.seconds = seconds;
    this.minutes = minutes;
    this.hours = hours;
    this.days = days;
  }


  /***
   * Returns the working time in seconds. Days, hours, minutes, seconds.
   *
   * @return all the variables {days,hours,minutes, seconds} converted to seconds.
   * */

  public Long getTimeSpent(){
    long dayToHours = convertDaysToHours(days);
    Long timeSpentInSeconds = TimeUnit.HOURS.toSeconds(dayToHours) +
      TimeUnit.HOURS.toSeconds(hours) +
      TimeUnit.MINUTES.toSeconds(minutes) + seconds;
    return timeSpentInSeconds;
  }


  /**
   * Method to convert worked days in hours.
   *  One day is equal to 8 working hours.
   *
   * @param days - days worked
   * @return  - returns hours worked from given day. One day = 8 hours.
   * */

  private long convertDaysToHours(Long days){
    return days*8;
  }

  public Long getTaskId () {
    return taskId;
  }

  public void setTaskId (Long taskId) {
    this.taskId = taskId;
  }


  public Long getSeconds () {
    return seconds;
  }

  public void setSeconds (Long seconds) {
    this.seconds = seconds;
  }

  public Long getMinutes () {
    return minutes;
  }

  public void setMinutes (Long minutes) {
    this.minutes = minutes;
  }

  public Long getHours () {
    return hours;
  }

  public void setHours (Long hours) {
    this.hours = hours;
  }

  public Long getDays () {
    return days;
  }

  public void setDays (Long days) {
    this.days = days;
  }

    /**
     * Prints the formatted logged time.
     * @return
     */
  public String print(){
    return String.format("%02dd %02dh %02dm %02ds", days, hours, minutes, seconds);
  }

  @Override
  public String toString () {
    return "LogHelper{" +
      ", taskId=" + taskId +
      ", seconds=" + seconds +
      ", minutes=" + minutes +
      ", hours=" + hours +
      ", days=" + days +
      '}';
  }
}
