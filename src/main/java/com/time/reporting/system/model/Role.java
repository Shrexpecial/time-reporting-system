package com.time.reporting.system.model;

import java.io.Serializable;

public enum Role implements Serializable {
  User,Admin
}
