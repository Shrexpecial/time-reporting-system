package com.time.reporting.system.model;

import java.io.Serializable;

public enum Severity implements Serializable {
  Urgent,High,Medium,Low
}
