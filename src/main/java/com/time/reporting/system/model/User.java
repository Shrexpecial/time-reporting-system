package com.time.reporting.system.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table (name="user")
public class User implements Serializable {

  @Id
  @GeneratedValue (strategy=GenerationType.IDENTITY)
  @Column (name="user_id")
  private Long id;

  @Column (name="username")
  @NotNull
  private String username;

  @Column(name="password")
  @NotNull
  private String password;

  @Column(name = "full_name")
  private String fullName;

  @Column(name="email")
  private String email;

  @Enumerated (EnumType.STRING)
  @Column(name="role")
  @NotNull
  private Role role;

  @ManyToMany (fetch = FetchType.EAGER,cascade ={CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable (
    name = "users_tasks",
    joinColumns = { @JoinColumn (name = "user_id") },
    inverseJoinColumns = { @JoinColumn(name = "task_id") }
  )
  private Set<Task> tasks = new HashSet<>(0);

  @OneToMany(cascade = CascadeType.ALL
          ,mappedBy = "user", orphanRemoval = true)
  @JsonIgnore
  private List<Log> log_entries;

  public User () {
  }

  public User (String username, String password, String fullName, String email, Role role) {
    this.username = username;
    this.password = password;
    this.fullName = fullName;
    this.email = email;
    this.role = role;
  }

  public Set<Task> getTasks () {
    return tasks;
  }

  public void setTasks (Set<Task> tasks) {
    this.tasks = tasks;
  }

  public void addTask(Task task){
    tasks.add(task);
  }

  public Long getId () {
    return id;
  }

  public void setId (Long id) {
    this.id = id;
  }

  public String getUsername () {
    return username;
  }

  public void setUsername (String username) {
    this.username = username;
  }

  public String getPassword () {
    return password;
  }

  public void setPassword (String password) {
    this.password = password;
  }

  public String getFullName () {
    return fullName;
  }

  public void setFullName (String fullName) {
    this.fullName = fullName;
  }

  public String getEmail () {
    return email;
  }

  public void setEmail (String email) {
    this.email = email;
  }

  public Role getRole () {
    return this.role;
  }

  public void setRole (Role role) {
    this.role = role;
  }

  public List<Log> getLog_entries() {
    return log_entries;
  }

  public void addLog_entry(Log entry){
    this.log_entries.add(entry);
  }

  public void setLog_entries(List<Log> log_entries) {
    this.log_entries.clear();
    this.log_entries.addAll(log_entries);
  }

  public boolean hasTask(Task task){
    for (Task current : tasks) {
      if (task.getId().equals(current.getId())) return true;
    }
    return false;
  }
  @Override
  public String toString () {
    return "User{" +
      "id=" + id +
      ", username='" + username + '\'' +
      ", password='" + password + '\'' +
      ", fullName='" + fullName + '\'' +
      ", email='" + email + '\'' +
      ", role=" + role +
      '}';
  }
}
