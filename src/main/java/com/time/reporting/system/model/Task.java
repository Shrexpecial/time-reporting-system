package com.time.reporting.system.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Entity object for Task table.
 * */

@Entity
@Table (name="task")
public class Task implements Serializable {

  @Id
  @GeneratedValue (strategy= GenerationType.IDENTITY)
  @Column (name="task_id")
  private Long id;

  @Column (name="name")
  @NotNull
  private String name;

  @Column(name = "description")
  private String description;

  @Enumerated (EnumType.STRING)
  @Column(name="severity")
  private Severity severity;

  @Column(name="time_spent")
  private Long timeSpent;

  @Column(name="modified")
  @Temporal (TemporalType.TIMESTAMP)
  @Past @NotNull
  private Calendar modified;

  @Column(name="created")
  @Temporal (TemporalType.TIMESTAMP)
  @Past @NotNull
  private Calendar created;

  @Column(name="reporter")
  @NotNull
  private String reporter;

  @ManyToMany (mappedBy = "tasks",cascade ={CascadeType.PERSIST}, fetch = FetchType.EAGER)
  @JsonIgnore
  private Set<User> users = new HashSet<>(0);

  @OneToMany(cascade = CascadeType.ALL
          ,mappedBy = "task", orphanRemoval = true)
  @JsonIgnore
  private List<Log> log_entries;

  /**
   * Initializing a task with default values
   * */

  public Task () {
    this.timeSpent = 0L;
    this.modified = Calendar.getInstance();
    this.created = Calendar.getInstance();
    this.severity = Severity.Medium;
  }

  /**
   * Initializing concrete task.
   *
   * */

  public Task (String name, String description, Severity severity,String reporter) {
    this();
    this.name = name;
    this.description = description;
    this.severity = severity;
    this.reporter = reporter;
  }

  public Set<User> getUsers () {
    return users;
  }

  public void setUsers (Set<User> users) {
    this.users = users;
  }

  public Long getId () {
    return id;
  }

  public void setId (Long id) {
    this.id = id;
  }

  public String getName () {
    return name;
  }

  public void setName (String name) {
    this.name = name;
  }

  public String getDescription () {
    return description;
  }

  public void setDescription (String description) {
    this.description = description;
  }

  public Severity getSeverity () {
    return severity;
  }

  public void setSeverity (Severity severity) {
    this.severity = severity;
  }

  public Long getTimeSpent () {
    return timeSpent;
  }

  public void setTimeSpent (Long timeSpent) {
    this.timeSpent = timeSpent;
  }

  public Calendar getModified () {
    return modified;
  }

  public void setModified (Calendar modified) {
    this.modified = modified;
  }

  public Calendar getCreated () {
    return created;
  }

  public void setCreated (Calendar created) {
    this.created = created;
  }

  public String getReporter () {
    return reporter;
  }

  public void setReporter (String reporter) {
    this.reporter = reporter;
  }


  public List<Log> getLog_entries() {
    return log_entries;
  }

  public void setLog_entries(List<Log> log_entries) {
    this.getLog_entries().clear();
    this.getLog_entries().addAll(log_entries);
  }

  public void updateTime(Long time){
    this.timeSpent += time;
    this.modified = Calendar.getInstance();
  }

  public void merge(Task task){
    setId(task.getId());
    setName(task.getName());
    setDescription(task.getDescription());
    setSeverity(task.getSeverity());
    setModified(Calendar.getInstance());
  }

  @Override
  public String toString () {
    return "Task{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", description='" + description + '\'' +
      ", severity=" + severity +
      ", timeSpent=" + timeSpent +
      ", modified=" + modified +
      ", created=" + created +
      ", reporter='" + reporter + '\'' +
      '}';
  }
}
