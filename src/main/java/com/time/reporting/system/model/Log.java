package com.time.reporting.system.model;

import com.time.reporting.system.utils.LogHelper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Entity
@Table(name = "log")
public class Log implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column (name = "log_id")
    private Long log_id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "task_id", nullable = false)
    private Task task;

    @Column(name = "time_spent")
    @NotNull
    private Long time_spent;

    @Column(name = "date")
    @Temporal (TemporalType.TIMESTAMP)
    @Past
    @NotNull
    private Calendar date;

    public Log() {
    }

    public Long getLog_id() {
        return log_id;
    }

    public void setLog_id(Long log_id) {
        this.log_id = log_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Long getTime_spent() {
        return time_spent;
    }

    public void setTime_spent(Long time_spent) {
        this.time_spent = time_spent;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar modified) {
        this.date = modified;
    }

    public Log(User user, Task task, Long time_spent, Calendar date) {
        this.user = user;
        this.task = task;
        this.time_spent = time_spent;
        this.date = date;
    }

    /**
     * Formats the contents of the log.
     * @return - the formatted log as a string
     */
    public String printLog(){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return "| " + format.format(this.date.getTime())+ " |" + " -> " +
                this.user.getUsername() + " spent "
                + new LogHelper(this.time_spent).print() + " working on "
                + this.task.getName()
                + " [" +this.task.getSeverity().toString() + "]";
    }

    @Override
    public String toString() {
        return ", User=" + user.getUsername() +
                ", Task=" + task.getName() +
                ", Time spent=" + time_spent +
                ", Date=" + date.getTime() +
                '}';
    }
}
