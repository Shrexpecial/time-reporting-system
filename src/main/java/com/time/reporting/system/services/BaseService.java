package com.time.reporting.system.services;

import com.time.reporting.system.controller.UserController;
import com.time.reporting.system.model.Role;
import com.time.reporting.system.model.User;
import com.time.reporting.system.utils.PasswordHelper;
import com.time.reporting.system.utils.SessionHelper;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Path("")
public class BaseService implements Serializable {

    @Inject
    private UserController userController;

    @Context
    private HttpServletRequest request;

    /**
     * GET .../
     * @return login page if no user is logged
     *          index page if admin is logged
     *          index-user page if user is logged
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public InputStream getIndex() {
        // validate if user is logged in
        String relativeWebPath;

        HttpSession session = request.getSession(false);
        if(SessionHelper.isAdminLogged(session)) { relativeWebPath = "/index.html"; }
        else if(SessionHelper.isUserLogged(session)) { relativeWebPath = "/index-user.html"; }
        else { relativeWebPath = "/login.html"; }

        return serveFile(relativeWebPath);
    }

    /**
     * GET .../{htmlfilename}
     * @param fileName - name of html file to be loaded
     * @return html file
     */
    @GET
    @Path("/{filename}")
    @Produces(MediaType.TEXT_HTML)
    public InputStream getPage(@PathParam("filename") String fileName) {
        String relativeWebPath;

        HttpSession session = request.getSession(false);
        if(SessionHelper.isAdminLogged(session)) { relativeWebPath = "/" + fileName + ".html"; }
        else if(SessionHelper.isUserLogged(session)) { relativeWebPath = "/index-user.html"; }
        else if(fileName.equals("register")) { relativeWebPath = "/register.html";    }
        else { relativeWebPath = "/login.html"; }

        return serveFile(relativeWebPath);
    }

    /**
     * GET .../css/{cssfilename}
     * @param fileName - name of css file to be loaded
     * @return css file
     */
    @GET
    @Path("/css/{filename}")
    public InputStream getStyle(@PathParam("filename") String fileName) {
        String relativeWebPath = "/css/" + fileName;
        return serveFile(relativeWebPath);
    }

    /**
     * GET .../js/{jsfilename}
     * @param fileName - name of css file to be loaded
     * @return js file
     */
    @GET
    @Path("/js/{filename}")
    public InputStream getScript(@PathParam("filename") String fileName) {
        String relativeWebPath = "/js/" + fileName;
        return serveFile(relativeWebPath);
    }

    /**
     * GET .../img/{imagefilename}
     * @param fileName - name of image file to be loaded
     * @return image file
     */
    @GET
    @Path("/img/{filename}")
    public InputStream getImage(@PathParam("filename") String fileName) {
        String relativeWebPath = "/img/" + fileName;
        return serveFile(relativeWebPath);
    }

    /**
     * POST .../login
     * @param user - consumes json with username, email and password
     * @return response if the user logged in successfully
     */
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(User user) {

        List<User> users = userController.findUserByUsernameOrEmail(user.getUsername(), user.getEmail());
        if(users.isEmpty()) {
            String message = "{\"message\": \"Wrong email or username\"}";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }

        User foundUser = users.get(0);
        try {
            if(PasswordHelper.validatePassword(user.getPassword(), foundUser.getPassword())) {
                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(60*60);

                session.setAttribute("id", foundUser.getId());
                session.setAttribute("role", foundUser.getRole());
            }
            else {
                String message = "{\"message\": \"Wrong password\"}";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        String message = "{\"message\": \"Successfully logged in\"}";
        return Response.ok(message).build();
    }

    /**
     * POST .../register
     *
     * Consumes json with user info and registers him in system
     */
    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(User user) {
        // username, password, fullName, email

        List<User> existingUser = userController.findUserByUsernameOrEmail(user.getUsername(), user.getEmail());
        if(existingUser.isEmpty()) {
            // hash password
            try {
                String hashedPassword = PasswordHelper.generatePasswordHash(user.getPassword());
                user.setPassword(hashedPassword);
                user.setRole(Role.User);
                userController.createUser(user);
            } catch (Exception e) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
        else {
            String message = "{\"message\": \"Account with this username or email already exists\"}";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }

        String message = "{\"message\": \"Successfully registered\"}";
        return Response.ok(message).build();
    }

    /**
     * GET .../account/logout
     *
     * Invalidates current session
     */
    @GET
    @Path("/account/logout")
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout() {
        HttpSession session = request.getSession(false);
        if(session != null) {
            session.invalidate();

            String message = "{\"message\": \"Successfully logged out\"}";
            return Response.ok(message).build();
        }
        else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * Helper method to serve files to client
     * @param relativePath - relative path to file
     * @return file via input stream
     */
    private InputStream serveFile(String relativePath) {
        String absoluteDiskPath = request.getServletContext().getRealPath(relativePath);
        File index = new File(absoluteDiskPath);

        try {
            return new FileInputStream(index);
        } catch (FileNotFoundException e) {
            String s = "ERROR";
            return new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
        }
    }
}
