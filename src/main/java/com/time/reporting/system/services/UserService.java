package com.time.reporting.system.services;

import com.time.reporting.system.controller.UserController;
import com.time.reporting.system.manager.MailNotificationManager;
import com.time.reporting.system.model.Log;
import com.time.reporting.system.utils.LogHelper;
import com.time.reporting.system.model.Task;
import com.time.reporting.system.model.User;
import com.time.reporting.system.utils.SessionHelper;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

@Path ("/user")
public class UserService implements Serializable {

  private static final String AUTHORIZATION_ERROR_MESSAGE = "{\"message\": \" You don't have permission to perform this operation. \"}";


  @Inject
  private UserController userController;
  @Inject
  private MailNotificationManager mailNotificationManager;

  @Context
  private HttpServletRequest request;

  /**
   * Get method.../user/{id}
   *
   * Search users_tasks by given id to find the tasks for the user
   *
   * @param id - id of the user
   * */

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAllTasks (@PathParam("id") @Min (1) Long id) {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isUserLogged(httpSession)) {
      Set<Task> allTasks = userController.findAllTasksByUser(id);
      if(allTasks.isEmpty()){
        String message = "{\"message\": \" Error occurred during retrieving all tasks. \"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      return Response.ok(allTasks).build();
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   * Get method.../user/tasks
   *
   * Search users_tasks by the user id to find the tasks for the user
   *
   * */

  @GET
  @Path("/tasks")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getTasksByUser () {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isUserLogged(httpSession)) {
      long currentId = (Long) httpSession.getAttribute("id");
      Set<Task> allTasks = userController.findAllTasksByUser(currentId);
      if(allTasks.isEmpty()){
        String message = "{\"message\": \" Error occurred during retrieving all tasks. \"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      return Response.ok(allTasks).build();
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   *
   * POST method .../user
   *
   * Allows user to log time they spent on some task.
   *
   * @param logHelper - Wrapper class that contains userId, taskId and time spent on the task.
   * */

  @POST
  @Consumes (MediaType.APPLICATION_JSON)
  @Produces (MediaType.APPLICATION_JSON)
  public Response saveLoggedTime(LogHelper logHelper) {
    HttpSession httpSession = request.getSession(false);
    if(SessionHelper.isUserLogged(httpSession)) {
      //Get user id from current session
      User user = userController.findUser((Long)httpSession.getAttribute("id"));

      //Get task id by the JSON
      Task task = userController.findTask(logHelper.getTaskId());

      //Log time
      Log log = new Log();
      log.setDate(Calendar.getInstance());
      log.setUser(user);
      log.setTask(task);

      log.setTime_spent(logHelper.getTimeSpent());
      boolean result = userController.logTime(log);
      if(result){
        //Mail service
        //Send mail to the reporter
        String email = userController.findReporter(task.getReporter()).getEmail();
        String topic = "Time Reporting System Task Notification";
        String textMessage = "The task  :" + task.getName() + " was changed!" + "\n User:" + user.getUsername() + "logged : " +
          logHelper.getTimeSpent();
        mailNotificationManager.send(email,topic,textMessage);

        //Send mail to user.
        mailNotificationManager.send(user.getEmail(),"Logged time"," You have successfully logged time!");

        // String message = "{\"message\": \" Successfully logged time. \"}";
        Task modifiedTask = userController.findTask(log.getTask().getId());
        return Response.ok().status(Response.Status.CREATED).entity(modifiedTask).build();
      }
      String message = "{\"message\": \" Error occurred during logging. \"}";
      return Response.ok().status(Response.Status.NOT_FOUND).entity(message).build();
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * GET .../user/current
   *
   * Returns user which is currently logged in 
   */
    @GET
    @Path("/current")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCurrentUser () {
        HttpSession httpSession = request.getSession(false);

        if(SessionHelper.isUserLogged(httpSession)) {
            Long currentId = (Long) httpSession.getAttribute("id");
            User currentUser = userController.findUser(currentId);
            if(currentUser == null){
                String message = "{\"message\": \"No such user\"}";
                return Response.status(Response.Status.NOT_FOUND).entity(message).build();
            }
            return Response.ok(currentUser).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
    }
}