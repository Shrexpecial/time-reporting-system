package com.time.reporting.system.services;

import com.time.reporting.system.controller.AdminController;
import com.time.reporting.system.manager.MailNotificationManager;
import com.time.reporting.system.model.Role;
import com.time.reporting.system.model.Severity;
import com.time.reporting.system.model.Task;
import com.time.reporting.system.model.User;
import com.time.reporting.system.utils.AssignTaskHelper;
import com.time.reporting.system.utils.ReportsHelper;
import com.time.reporting.system.utils.ReportsRequestHelper;
import com.time.reporting.system.utils.SessionHelper;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Path ("/admin")
public class AdminService implements Serializable {

  private static final String AUTHORIZATION_ERROR_MESSAGE = "{\"message\": \" You don't have permission to perform this operation. \"}";

  @Inject
  private AdminController adminController;
  @Inject
  private MailNotificationManager mailNotificationManager;

  @Context
  private HttpServletRequest request;

  /**
   * GET .../admin/report/full
   *
   * Returns pdf with report for time spent by all users on all tasks
   */
  @GET
  @Path("/report/full")
  @Produces("application/pdf")
  public Response getFullReport(){
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      ReportsHelper helper = adminController.
              getReport(adminController.findAllTasks(), adminController.findAllUsers());
      return serveReport(helper);
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   * GET .../admin/report/severity/{severity}
   *
   * Returns pdf with severity report for specific severity
   *
   */
  @GET
  @Path("/report/severity/{severity}")
  @Produces("application/pdf")
  public Response getSeverityReport(@PathParam ("severity") @NotNull Severity severity){
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      ReportsHelper helper = adminController.getSeverityReport(severity);
      return serveReport(helper);
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * POST .../admin/report
   *
   * Returns pdf with report for time spent on given list of tasks by list of users
   */
  @POST
  @Path("/report")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces("application/pdf")
  public Response getReport(ReportsRequestHelper requestHelper){
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      List<Long> userIDs = requestHelper.getUserIDs();
      List<User> users = userIDs.stream()
              .map(id -> adminController.findUser(id))
              .collect(Collectors.toList());

      List<Long> taskIDs = requestHelper.getTaskIDs();
      List<Task> tasks = taskIDs.stream()
              .map(id -> adminController.findTask(id))
              .collect(Collectors.toList());
      ReportsHelper helper = adminController.getReport(tasks, users);
      return serveReport(helper);
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * GET - ..admin/users
   *
   * Returns all users with their tasks in json format
   * */

  @GET
  @Path("/users")
  @Produces (MediaType.APPLICATION_JSON)
  public Response getAllUsers(){
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      List<User> allUsers = adminController.findAllUsers();

      if(allUsers.isEmpty()){
        String message = "{\"message\": \" No users found. \"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      return Response.ok(allUsers).build();

    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * GET - ..admin/tasks
   *
   * Returns all users with their tasks in json format
   * */

  @GET
  @Path("/tasks")
  @Produces (MediaType.APPLICATION_JSON)
  public Response getAllTasks(){
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      List<Task> allTasks = adminController.findAllTasks();

      if(allTasks.isEmpty()){
        String message = "{\"message\": \" No tasks found. \"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      return Response.ok(allTasks).build();

    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   * POST method .../admin
   *
   * Allows administrator to create task.
   *
   * @param task - Task class containing name, description, severity, time spent reporter.
   * */

  @POST
  @Consumes (MediaType.APPLICATION_JSON)
  @Produces (MediaType.APPLICATION_JSON)
  public Response createTask(Task task, @Context UriInfo uriInfo) {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      long currentId = (Long) httpSession.getAttribute("id");
      User reporter = adminController.findUser(currentId);
      task.setReporter(reporter.getUsername());
      adminController.createTask(task);

      //Mail service
      String email = reporter.getEmail();
      String topic = "Time Reporting System Task Notification";
      String textMessage = "You have created task " + task.getName();
      mailNotificationManager.send(email,topic,textMessage);

      String message = "{\"message\": \" The task is successfully created! \"}";
      return Response.ok().entity(task).build();
    }
    URI createdURI = uriInfo.getBaseUriBuilder().path(String.valueOf(task.getId())).build();
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   * POST ... /admin/assignment
   *
   * Assign users to task.
   *
   * JSON Format - List<User>, Long taskId
   *
   * */

  @POST
  @Path ("/assignment")
  @Consumes (MediaType.APPLICATION_JSON)
  @Produces (MediaType.APPLICATION_JSON)
  public Response assignUserToTask(AssignTaskHelper assignTaskHelper){

    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      //Get list of users
      List<Long> usersID = assignTaskHelper.getUsersID();
      List<User> users = usersID.stream()
        .map(id -> adminController.findUser(id))
        .collect(Collectors.toList());

      //Get task id
      Long taskId = assignTaskHelper.getTaskId();

      //From the task id, get task
      Task task = adminController.findTask(taskId);
      //For each user, add task.
      users.forEach(user -> adminController.addTaskToUser(user.getId(),task));

      //Mail service
      String email = adminController.findUser((Long)httpSession.getAttribute("id")).getEmail();
      String topic = "Time Reporting System Task Notification";
      String textMessage = "You have assigned user :" + users + " to task : " + task.getName();
      mailNotificationManager.send(email,topic,textMessage);
      for (User user:
           users) {
        mailNotificationManager.send(user.getEmail(),topic,"You have been assigned to task: " + task.getName());
      }
      String message = "{\"message\": \" Successfully assigned task to the users. \"}";
      return Response.ok().entity(message).build();
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * DELETE ... /admin/{id}
   *
   * Delete task by id.
   *
   * */

  @DELETE
  @Path("/task/{id}")
  public Response deleteTask(@PathParam ("id") @Min (1) Long id){
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {

      //Mail service
      String email = adminController.findUser((Long)httpSession.getAttribute("id")).getEmail();
      String topic = "Time Reporting System Task Notification";
      String textMessage = "The task  :" + adminController.findTask(id).getName() + " was deleted!";
      mailNotificationManager.send(email,topic,textMessage);

      Task task = adminController.findTask(id);
      Set<User> users = task.getUsers();
      for (User user:
        users) {
        mailNotificationManager.send(user.getEmail(),topic,"The task " + task.getName() + " was deleted");
      }

      adminController.deleteTask(id);

      if(adminController.findTask(id) != null){
        String message = "{\"message\": \" Couldn't delete task with id:" + id + "\"}";
        Response.ok().status(Response.Status.NOT_MODIFIED).entity(message).build();
      }
      String message = "{\"message\": \" Successfully found task with id:" + id + "\"}";
      return Response.ok().status(Response.Status.OK).entity(message).build();
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   * POST ... /admin/update
   *
   * Update task.
   *
   * */

  @POST
  @Path("/update")
  @Consumes (MediaType.APPLICATION_JSON)
  @Produces (MediaType.APPLICATION_JSON)
  public Response updateTask(Task task) {
    HttpSession httpSession = request.getSession(false);

    if (SessionHelper.isAdminLogged(httpSession)) {
      long currentId = (Long) httpSession.getAttribute("id");
      User reporter = adminController.findUser(currentId);
      adminController.updateTask(task);

      //Mail service
      String email = reporter.getEmail();
      String topic = "Time Reporting System Task Notification";
      String textMessage = "The task " + task.getName() + " is updated.";
      mailNotificationManager.send(email, topic, textMessage);

      String message = "{\"message\": \" The task is successfully updated! \"}";
      return Response.ok().entity(task).build();
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  @Path("/users/task/{id}")
  @GET
  @Produces (MediaType.APPLICATION_JSON)
  public Response getUsersByTask(@PathParam ("id") @Min (1) Long id) {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      Set<User> users = adminController.findAllUsersByTask(id);
      if(users.isEmpty()) {
        String message = "{\"message\": \" No users assigned to this task:" + id + "\"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      else {
        return Response.ok(users).build();
      }
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }


  /**
   * GET .../admin/users/not/task/{id}
   *
   * Returns list of users which are not assigned to task with {id}
   */
  @Path("/users/not/task/{id}")
  @GET
  @Produces (MediaType.APPLICATION_JSON)
  public Response getNotAssignedUsers(@PathParam ("id") @Min (1) Long id) {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      Set<User> assignedUsers = adminController.findAllUsersByTask(id);
      List<User> allUsers = adminController.findAllUsers();

      List<User> notAssignedUsers = allUsers.stream()
              .filter(user -> {
                for(User assignedUser : assignedUsers) {
                  if(assignedUser.getId().equals(user.getId()))
                    return false;
                }
                return true;
              })
              .collect(Collectors.toList());

      if(notAssignedUsers.isEmpty()) {
        String message = "{\"message\": \" All users assigned to this task:" + id + "\"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      else {
        return Response.ok(notAssignedUsers).build();
      }
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * GET .../admin/tasks/not/user/{id}
   *
   * Returns list of tasks which users with {id} is not assigned to
   */
  @Path("/tasks/not/user/{id}")
  @GET
  @Produces (MediaType.APPLICATION_JSON)
  public Response getNotAssignedTasks(@PathParam ("id") @Min (1) Long id) {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      Set<Task> assignedTasks = adminController.findAllTasksByUser(id);
      List<Task> allTasks = adminController.findAllTasks();

      List<Task> notAssignedTasks = allTasks.stream()
              .filter(task -> {
                for(Task assignedTask : assignedTasks) {
                  if(assignedTask.getId().equals(task.getId()))
                    return false;
                }
                return true;
              })
              .collect(Collectors.toList());

      if(notAssignedTasks.isEmpty()) {
        String message = "{\"message\": \" All tasks are assigned to this user:" + id + "\"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }
      else {
        return Response.ok(notAssignedTasks).build();
      }
    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * GET .../admin/promotion/{id}
   *
   * Changes user's role to Admin
   */
  @Path("/promotion/{id}")
  @GET
  @Produces (MediaType.APPLICATION_JSON)
  public Response promoteToAdmin(@PathParam ("id") @Min (1) Long id) {
    HttpSession httpSession = request.getSession(false);

    if(SessionHelper.isAdminLogged(httpSession)) {
      User user = adminController.findUser(id);

      if(user == null) {
        String message = "{\"message\": \"No user found with id " + id + "\"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }

      if(user.getRole().equals(Role.Admin)) {
        String message = "{\"message\": \"User with id " + id + " is already  Admin\"}";
        return Response.status(Response.Status.CONFLICT).entity(message).build();
      }

      adminController.changeUserPrivileges(user, Role.Admin);
      String message = "{\"message\": \"User with id " + id +" is promoted to  Admin\"}";
      return Response.ok(message).build();

    }
    return Response.status(Response.Status.BAD_REQUEST).entity(AUTHORIZATION_ERROR_MESSAGE).build();
  }

  /**
   * Helper method to serve pdf report files to client
   */
  private Response serveReport(ReportsHelper helper) {
    try {
      helper.createPDFReport(request.getServletContext().getRealPath("/"));
      String relativePath = "/reports/Report.pdf";
      String absoluteDiskPath = request.getServletContext().getRealPath(relativePath);
      File index = new File(absoluteDiskPath);

      try {
        Response.ResponseBuilder responseBuilder = Response.ok(new FileInputStream(index));
        responseBuilder.type("application/pdf");
        responseBuilder.header("Content-Disposition", "filename=Report.pdf");
        return responseBuilder.build();
      }
      catch (FileNotFoundException e) {
        String message = "{\"message\": \" Could not locate a report. \"}";
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
      }

    }
    catch (Exception e) {
      e.printStackTrace();
      String message = "{\"message\": \" Could not generate a report. \"}";
      return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
    }
  }
}
