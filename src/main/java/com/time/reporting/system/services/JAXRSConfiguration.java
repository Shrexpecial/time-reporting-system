package com.time.reporting.system.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.Serializable;

@ApplicationPath("/")
public class JAXRSConfiguration extends Application implements Serializable {
  public JAXRSConfiguration() {}
}
