package com.time.reporting.system.controller;

import com.time.reporting.system.model.Log;
import com.time.reporting.system.model.Task;
import com.time.reporting.system.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static javax.transaction.Transactional.TxType.REQUIRED;
import static javax.transaction.Transactional.TxType.SUPPORTS;

@Transactional (SUPPORTS)
public class UserController implements Serializable {

  @PersistenceContext (unitName = "UserPU")
  private static EntityManager em;

  /**
   * Creates a user
   *
   * @param user - with the given parameter, the method persist the entity in the database
   * @return User
   * */
  @Transactional(REQUIRED)
  public User createUser(@NotNull User user){
    em.persist(user);
    return user;
  }

  /**
   * Find user by given id.
   *
   * @param id - find user by the parameter id
   * @return User
   * */

  @Transactional (SUPPORTS)
  public User findUser(@NotNull Long id) {
    return em.find(User.class,id);
  }

  /**
   * Find user by given username or email.
   *
   * @param username - find user by the parameter username
   * @param email - or find user by email
   * */

  @Transactional (SUPPORTS)
  public List<User> findUserByUsernameOrEmail(@NotNull String username, @NotNull String email) {
    TypedQuery<User> query = em.createQuery("select user from User user " +
            "where user.username = (:username) or user.email = (:email)", User.class)
            .setParameter("username", username)
            .setParameter("email", email);

    return query.getResultList();
  }

  /**
   * Find task by given id
   *
   * @param id - id of the task
   * @return Task - the task that was found
   * */

  @Transactional (SUPPORTS)
  public Task findTask (@NotNull Long id) {
    return em.find(Task.class, id);
  }

    /**
     * Persists a log in the Log table
     * @param log - the log you wish to persist
     * @return - whether the log was persisted
     */
  @Transactional(REQUIRED)
  public boolean logTime(@NotNull Log log){
    User user = log.getUser();
    Task task = log.getTask();
    if (user.hasTask(task)){
      task.updateTime(log.getTime_spent());
      em.persist(log);
      em.merge(task);
      return true;
    }
    return false;
  }


  /**
   * Given the user id, method returns set of tasks related to the user
   *
   * @param id - task id
   * @return Set<Task> - all the tasks related to the user
   * */

  public Set<Task> findAllTasksByUser (Long id) {
    User user = findUser(id);
    return user.getTasks();
  }


  /**
   * Given reporter name as string, method returns reporter user class.
   *
   * @param reporter - Username of the reporter.
   * @return User - returns empty user if no reporter with that name is found, else
   *        returns reporter user class.
   * */
  public User findReporter(String reporter) {
    List<User> userByUsernameOrEmail = findUserByUsernameOrEmail(reporter, "");
    for(User user : userByUsernameOrEmail){
      if(user.getUsername().equals(reporter)){
        return user;
      }
    }
    return new User();
  }
}
