package com.time.reporting.system.controller;

import com.time.reporting.system.model.Log;
import com.time.reporting.system.model.Role;
import com.time.reporting.system.model.Severity;
import com.time.reporting.system.model.Task;
import com.time.reporting.system.model.User;
import com.time.reporting.system.utils.ReportsHelper;

import javax.enterprise.inject.Specializes;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static javax.transaction.Transactional.TxType.REQUIRED;

@Specializes
public class AdminController extends UserController implements Serializable {

  @PersistenceContext (unitName = "UserPU")
  private static EntityManager em;

  /**
   * Creates a task
   *
   * @param task - entity to be persisted in the database
   */
  @Transactional (REQUIRED)
  public Task createTask (@NotNull Task task) {
    em.persist(task);
    return task;
  }

  /***
   * Update task in the database
   *
   * @param task - update the task that exist in the database with the new task
   */

  @Transactional(REQUIRED)
  public Task updateTask(@NotNull Task task){
    Task existingTask = em.find(Task.class,task.getId());
    if(existingTask != null){
      existingTask.merge(task);
    }
    return existingTask;
  }

  /***
   * Delete task from the database
   *
   * Performs searching for the task and then deletes it from the users set of tasks.
   *
   * @param id - task id
   */

  @Transactional(REQUIRED)
  public void deleteTask(@NotNull @Min(1) @Max(1000)Long id){
    Task task = this.findTask(id);
    var users = task.getUsers();
    for (User user : users) {
      user.getTasks().remove(task);
    }
    em.remove(task);
  }

  /**
   * Filters the Log db based on given users and tasks.
   * @param tasks - ids of the tasks we wish to track
   * @param users - ids of the users we wish to track
   * @return a list of all log entries satisfying the given criterion
   *  as well as the time spent for each user on a specific task
   */

  public ReportsHelper getReport(List<Task> tasks, List<User> users) {
    ReportsHelper reports = new ReportsHelper();
    TypedQuery<Log> query = em.createQuery("SELECT log from Log log where " +
            "log.user in (:users) and log.task in (:tasks) order by log.date", Log.class)
            .setParameter("users", users)
            .setParameter("tasks", tasks);
    reports.setLogs(query.getResultList());
    for (User user : users) {
      Long totalTime = 0L;
      for (Task task : tasks) {
        TypedQuery<Long> sum = em.createQuery("SELECT sum(log.time_spent) " +
                "as total from Log log where log.user = :user and log.task = :task", Long.class).
                setParameter("user", user)
                .setParameter("task", task);
        Long timeForTask = sum.getSingleResult();
        if(timeForTask != null){
          totalTime += timeForTask;
          reports.addTime(user, task, timeForTask);
        }
      }
      Task total = new Task();
      reports.addTime(user, total, totalTime);
    }
    return reports;
  }

  /**
   * Generates a full report for the log entries.
   * @return A list of all log entries present in the Log db
   */

  public ReportsHelper getFullReport(){
    TypedQuery<Log> query = em.createQuery("select log from Log log order by log.date", Log.class);
    ReportsHelper helper = new ReportsHelper();
    helper.setLogs(query.getResultList());
    return helper;
  }

  /**
   * Generate a report based on task severity
   * @param severity - desired severities
   * @return A list of all log entries satisfying the given criterion
   */

  public ReportsHelper getSeverityReport(Severity severity){
    ReportsHelper report = new ReportsHelper();
    TypedQuery<Log> query = em.createQuery("select log from Log log " +
            "where log.task.severity = :severity order by log.date", Log.class)
            .setParameter("severity", severity);
    report.setLogs(query.getResultList());
    for (User user : this.findAllUsers()) {
      Long totalTime = 0L;
      for (Task task : this.findAllTasks()) {
        TypedQuery<Long> sum = em.createQuery("SELECT sum(log.time_spent) " +
                "as total from Log log where log.user = :user and log.task = :task " +
                "and task.severity = :severity", Long.class).
                setParameter("user", user)
                .setParameter("task", task)
                .setParameter("severity", severity);
        Long timeForTask = sum.getSingleResult();
        if(timeForTask != null){
          totalTime += timeForTask;
          report.addTime(user, task, timeForTask);
        }
      }
      Task total = new Task();
      report.addTime(user, total, totalTime);
    }
    return report;
  }

  /**
   * Assigns a user to a particular task.
   * Checks whether the user is currently assigned
   * to the task before adding it to his set of tasks.
   * @param id - the user id
   * @param task - the task id
   */

  @Transactional(REQUIRED)
  public void addTaskToUser(Long id, Task task){
    User user = findUser(id);
    boolean contains = user.hasTask(task);
    if (!contains){
      user.addTask(task);
      em.merge(user);
    }
  }

  /**
   * Allowing the admin to change the role of a user.
   * @param user - user to change the role of
   * @param role - new role
   */

  @Transactional(REQUIRED)
  public void changeUserPrivileges(User user, Role role){
    user.setRole(role);
    em.merge(user);
  }


  /**
   * Method to retrieve all users from the database
   * @return list of users
   */

  public List<User> findAllUsers () {
    TypedQuery<User> query = em.createQuery("SELECT user FROM User user", User.class);
    return query.getResultList();
  }

  /**
   * Method to retrieve all tasks from the database
   * @return list of tasks
   */
  public List<Task> findAllTasks(){
    TypedQuery<Task> query = em.createQuery("SELECT task FROM Task task", Task.class);
    return query.getResultList();
  }

  public Set<User> findAllUsersByTask(Long id) {
    Task task = findTask(id);
    return task.getUsers();
  }
}
