const logoutBtn = document.querySelector("#logout");
logoutBtn.addEventListener("click", (e) => {
    e.preventDefault();

    getData(rootUrl + "/account/logout")
        .then(result => {
            console.log(result);
            window.location.replace(rootUrl + "/login");
        }).catch(error => console.error(error));
});