let report;

window.onload = () => {
    rootUrl = window.location.protocol + '//' + window.location.host + "/Time-Reporting-System-1.0";

    getData(rootUrl + "/user/current")
        .then(result => {
            document.querySelector("#username").innerHTML = result.username;
        }).catch(error => console.error(error));
};

// MODAL MANAGEMENT
$('#report-modal').on('show.bs.modal', function (event) {
    $('#full-report').hide();
    $('#viewReport').hide();

    var button = $(event.relatedTarget); // Button that triggered the modal 
    report = button.data('report');
    var modalButton = $('#modalBtn');
    modalButton.attr('data-report', report);
    deleteOptions();

    if (report == "user-task") {
        // Time spent on a task by specific user
        $('#task').show();
        getData(rootUrl + "/admin/tasks")
            .then(result => {
                result.forEach(task => addOptionTask(task, 'task'));
            })
            .catch(error => {
                console.error(error);
            });

        $('#user').show();

        $('#tasks').hide();
        $('#users').hide();
        $('#severity').hide();
    }
    else if (report == "user-tasks") {
        // Time spent on all tasks by a user

        $('#user').show();
        getData(rootUrl + "/admin/users")
            .then(result => {
                result.forEach(user => addOptionUser(user, 'user'));
            })
            .catch(error => {
                console.error(error);
            });

        $('#tasks').show();

        $('#task').hide();
        $('#users').hide();
        $('#severity').hide();

    }
    else if (report == "users-task") {
        // Time spent on a task by all users
        $('#task').show();
        getData(rootUrl + "/admin/tasks")
            .then(result => {
                result.forEach(task => addOptionTask(task, 'task'));
            })
            .catch(error => {
                console.error(error);
            });

        $('#users').show();

        $('#tasks').hide();
        $('#user').hide();
        $('#severity').hide();
    }
    else if (report == "users-tasks") {
        // Time spent by all users on all tasks
        // TODO : different way
        $('#tasks').hide();
        $('#users').hide();
        $('#task').hide();
        $('#user').hide();
        $('#severity').hide();

        let modal = $(this);
        modal.find('.modal-body').append("<p id='full-report'>Are you sure you want to generate a full report?</p>");
    }
    else if (report == "severity") {
        // Time spent on all tasks with specific severity
        $('#severity').show();

        $('#task').hide();
        $('#user').hide();
        $('#tasks').hide();
        $('#users').hide();
    }
});

function addOptionUser(user, selectId) {
    userSelect = document.getElementById(selectId);
    userSelect.options.add(new Option(user.username, user.id));
}

function addOptionTask(task, selectId) {
    taskSelect = document.getElementById(selectId);
    taskSelect.options.add(new Option(task.name, task.id));
}

function deleteOptions() {
    $("#user").empty();
    $("#users").empty();
    $("#task").empty();
    $("#tasks").empty();
}

// EVENT LISTENERS
let taskSelect = document.querySelector('#task');
taskSelect.addEventListener('change', (e) => {
    $("#user").empty();
    $("#users").empty();

    getData(rootUrl + "/admin/users/task/" + taskSelect.value)
        .then(result => {
            result.forEach(user => {
                if (report == "user-task") { addOptionUser(user, 'user'); }
                else if (report == "users-task") { addOptionUser(user, 'users'); }
            });
        }).catch(error => {
            console.error(error);
        });
});

let userSelect = document.querySelector('#user');
userSelect.addEventListener('change', (e) => {
    $("#tasks").empty();

    getData(rootUrl + "/user/" + userSelect.value)
        .then(result => {
            result.forEach(task => addOptionTask(task, 'tasks'));
        }).catch(error => {
            console.error(error);
        });
});

let modalBtn = document.querySelector('#modalBtn');
modalBtn.addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelector("#errors").innerHTML = "";

    if (e.target.getAttribute('data-report') === 'user-task') {
        let userID = document.querySelector('#user').value;
        let taskID = document.querySelector('#task').value;

        if (isValidReportsData(userID, taskID)) {
            let data = { "userIDs": [userID], "taskIDs": [taskID] };
            console.log(data);
            postReportData(rootUrl + "/admin/report", data)
                .then(result => {
                    showPdf(result);
                }).catch(error => {
                    console.error(error);
                });
        }
    }
    else if (e.target.getAttribute('data-report') === 'user-tasks') {
        let userID = document.querySelector('#user').value;
        let taskIDs = $('#tasks').val();

        if (isValidReportsData(userID, taskIDs)) {
            let data = { "userIDs": [userID], "taskIDs": taskIDs };
            postReportData(rootUrl + "/admin/report", data)
                .then(result => {
                    showPdf(result);
                }).catch(error => {
                    console.error(error);
                });
        }
    }
    else if (e.target.getAttribute('data-report') === 'users-task') {
        let userIDs = $('#users').val();
        let taskID = document.querySelector('#task').value;

        if (isValidReportsData(userIDs, taskID)) {
            let data = { "userIDs": userIDs, "taskIDs": [taskID] };
            postReportData(rootUrl + "/admin/report", data)
                .then(result => {
                    showPdf(result);
                }).catch(error => {
                    console.error(error);
                });
        }
    }
    else if (e.target.getAttribute('data-report') === 'users-tasks') {
        getReport(rootUrl + "/admin/report/full")
            .then(result => {
                showPdf(result);
            }).catch(error => {
                console.error(error);
            });
    }
    else if (e.target.getAttribute('data-report') === 'severity') {
        let severity = document.querySelector('#severity').value;
        getReport(rootUrl + "/admin/report/severity/" + severity)
            .then(result => {
                showPdf(result);
            }).catch(error => {
                console.error(error);
            });

    }

});

// VALIDATION
function isValidReportsData(users, tasks) {
    let isValidUser = validate(users, "User/s");
    let isValidTask = validate(tasks, "Task/s");

    return isValidUser && isValidTask;
}

function validate(data, string) {
    if (data == null || data == "" || data.length == 0) {
        displayError(string + " is required");
        return false;
    }
    return true;
}

function showPdf(blob) {
    $('#report-modal').modal('hide');

    let newBlob = new Blob([blob], { type: "application/pdf" });

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
    }

    const filePath = window.URL.createObjectURL(newBlob);
    // window.open(data);
    let anchor = document.createElement("a");
    anchor.setAttribute("target", "_blank");
    anchor.href = filePath;
    anchor.download = "Report.pdf";
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
}