const registerBtn = document.querySelector("#register");
registerBtn.addEventListener("click", (e) => {
    e.preventDefault();
    document.querySelector("#errors").innerHTML = "";

    const email = document.querySelector("#email").value;
    const username = document.querySelector("#username").value;
    const fullName = document.querySelector("#fullName").value;
    const password = document.querySelector("#password").value;
    const passwordRepeat = document.querySelector("#passwordRepeat").value;

    if (isValidData(email, username, fullName, password, passwordRepeat)) {
        const data = { "email": email, "username": username, "fullName": fullName, "password": password };
        postData(rootUrl + "/register", data)
            .then(result => {
                console.log(result);
                window.location.replace(rootUrl + "/login");
                alert("You registered successfully! Login with your username/email and password.");
            }).catch(error => {
                console.error(error);
                displayError(error.message);
            });
    }
});

function isValidData(email, username, fullName, password, passwordRepeat) {
    let isValidEmail = validateEmail(email);
    let isValidUsername = validateUsername(username);
    let isValidFullName = validateFullName(fullName);
    let isValidPassword = validatePassword(password, passwordRepeat);

    return isValidEmail &&
        isValidUsername &&
        isValidFullName &&
        isValidPassword;
}

function validateEmail(email) {
    if (email == "") {
        displayError("Email is required");
        return false;
    }

    if (!isValidEmail(email)) {
        displayError("Email is not valid");
        return false;
    }

    return true;
}

function validateUsername(username) {
    if (username == "") {
        displayError("Username is required");
        return false;
    }

    if (username.length < 3) {
        displayError("Username must be at least 3 characters long");
        return false;
    }

    let usernamePattern = new RegExp("^[a-zA-Z0-9_]+$");
    if (!usernamePattern.test(username)) {
        displayError("Username may only contain letters, numbers and underscores");
        return false;
    }

    return true;
}

function validateFullName(fullName) {
    if (fullName == "") {
        displayError("Full name is required");
        return false;
    }

    return true;
}

function validatePassword(password, passwordRepeat) {
    if (password == "") {
        displayError("Password is required");
        return false;
    }

    if (password.length < 6) {
        displayError("Password must be at least 6 characters long");
        return false;
    }

    // REPEATED PASSWORD VALIDATION
    if (password !== passwordRepeat) {
        displayError("Passwords do not match");
        return false;
    }

    return true;
}