let currentRow;
let currentUserId;

// LOAD TASKS
window.onload = () => {
    rootUrl = window.location.protocol + '//' + window.location.host + "/Time-Reporting-System-1.0";

    getData(rootUrl + "/user/current")
        .then(result => {
            document.querySelector("#username").innerHTML = result.username;
        }).catch(error => console.error(error));

    loadUsers();
};

function loadUsers() {
    getData(rootUrl + "/admin/users")
        .then(result => {
            result.forEach(user => {
                createRow(user);
            });
        })
        .catch(error => {
            console.error(error);
        });
}

function createRow(user) {
    let table = document.getElementById('users-table').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow();

    let idCell = newRow.insertCell(0);
    idCell.innerHTML = user.id;

    let usernameCell = newRow.insertCell(1);
    usernameCell.innerHTML = user.username;

    let fullNameCell = newRow.insertCell(2);
    fullNameCell.innerHTML = user.fullName;

    let emailCell = newRow.insertCell(3);
    emailCell.innerHTML = user.email;

    let roleCell = newRow.insertCell(4);
    roleCell.innerHTML = user.role;

    let actionsCell = newRow.insertCell(5);
    let btnTemplate = document.querySelector('#buttons');
    let buttons = btnTemplate.content.cloneNode(true);
    actionsCell.appendChild(buttons);
    feather.replace();
}

// VIEW TASKS
$('#view-modal').on('show.bs.modal', function (event) {
    deleteAllRows();

    var button = $(event.relatedTarget);
    var parent = button.parent().parent();
    var tdId = parent.children("td:nth-child(1)").text();

    getData(rootUrl + "/user/" + tdId)
        .then(result => {
            console.log(result);
            result.forEach(task => createTaskInTable(task));
        }).catch(error => {
            console.error(error);
        });

});

function createTaskInTable(task) {
    let table = document.getElementById('tasks-table').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow();

    let idCell = newRow.insertCell(0);
    idCell.innerHTML = task.id;

    let nameCell = newRow.insertCell(1);
    nameCell.innerHTML = task.name;

    let descrCell = newRow.insertCell(2);
    descrCell.innerHTML = task.description;

    let severityCell = newRow.insertCell(3);
    severityCell.innerHTML = task.severity;
}

function deleteAllRows() {
    let table = document.getElementById('tasks-table').getElementsByTagName('tbody')[0];
    let rowsNumber = table.rows.length;

    while (rowsNumber > 0) {
        table.deleteRow(-1);
        rowsNumber--;
    }
}

// ASSIGN TASK
$('#assign-modal').on('show.bs.modal', function (event) {
    deleteOptions();

    var button = $(event.relatedTarget);
    var parent = button.parent().parent();
    var tdId = parent.children("td:nth-child(1)").text();
    currentUserId = tdId;

    getData(rootUrl + "/admin/tasks/not/user/" + tdId)
        .then(result => {
            console.log(result);
            result.forEach(task => addOption(task));
        }).catch(error => {
            console.error(error);
        });

});

function addOption(task) {
    taskSelect = document.getElementById('tasks');
    taskSelect.options.add(new Option(task.name, task.id));
}

function deleteOptions() {
    $("#tasks").empty();
}

let assignBtn = document.querySelector('#assignBtn');
assignBtn.addEventListener('click', (e) => {
    e.preventDefault();

    let taskId = document.querySelector('#tasks').value;
    let data = { "usersID": [currentUserId], "taskId": taskId };
    postData(rootUrl + "/admin/assignment", data)
        .then(result => {
            console.log(result);
            alert("You successfully assigned user to task");

            // $('#time-modal').modal('hide');

        }).catch(error => {
            console.error(error);
        });

});

// PROMOTE TO ADMIN
$('#admin-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var parent = button.parent().parent();
    currentRow = parent;

    var tdId = parent.children("td:nth-child(1)").text();
    currentUserId = tdId;
});

let adminBtn = document.querySelector('#adminBtn');
adminBtn.addEventListener('click', (e) => {
    e.preventDefault();

    let id = currentUserId;
    getData(rootUrl + "/admin/promotion/" + id)
        .then(result => {
            console.log(result);
            updateRole();

            $('#admin-modal').modal('hide');
        }).catch(error => {
            console.error(error);
            $('#admin-modal').modal('hide');
            alert(error.message);
        });

});

function updateRole() {
    currentRow.children("td:nth-child(5)").text("Admin");
}