/**
 * requests.js - contains functions for sending http requests
 */

// variable to keep the root url for the webapp (ex. localhost:8080/Time-Reporting-System-1.0)
let rootUrl;

window.onload = () => {
    rootUrl = window.location.protocol + '//' + window.location.host + "/Time-Reporting-System-1.0";
};

/**
 * sends POST request with body
 * @param {*string} url - path for sending request to server
 * @param {*json object} data - data to send in request body
 */
function postData(url = '', data = {}) {
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(response => {
            if (response.ok) { return response.json(); }
            else { return response.json().then(error => Promise.reject(error)); }
        });
}

/**
 * sends GET request
 * @param {string} url - url for sending request to server
 */
function getData(url = '') {
    return fetch(url)
        .then(response => {
            if (response.ok) { return response.json(); }
            else { return response.json().then(error => Promise.reject(error)); }
        });
}

/**
 * sends DELETE request
 * @param {string} url - path for sending request to server 
 */
function deleteData(url = '') {
    return fetch(url, {
        method: 'DELETE'
    })
        .then(response => {
            if (response.ok) { return response.json(); }
            else { return response.json().then(error => Promise.reject(error)); }
        });
}

/**
 * sends GET request, and expects blob reponse (pdf file)
 * @param {string} url - path for sending request to server 
 */
function getReport(url = '') {
    return fetch(url)
        .then(response => {
            if (response.ok) { return response.blob(); }
            else { return response.json().then(error => Promise.reject(error)); }
        });
}

/**
 * sends POST request, and expects blob reponse (pdf file)
 * @param {string} url - path for sending request to server 
 */
function postReportData(url = '', data = {}) {
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(response => {
            if (response.ok) { return response.blob(); }
            else { return response.json().then(error => Promise.reject(error)); }
        });
}

function isValidEmail(email) {
    let regEx = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regEx.test(email)
}

function displayError(errorMessage) {
    let errorDiv = document.querySelector("#errors");
    let errorP = document.createElement("p");
    errorP.innerText = errorMessage;
    errorDiv.appendChild(errorP);
}

function secondsToTime(totalTimeSpent) {
    let time = parseFloat(totalTimeSpent).toFixed(3);
    let days = Math.floor(time / 28800); // 1 day = 8h
    time %= 28800;

    let hours = Math.floor(time / 3600);
    time %= 3600;

    let minutes = Math.floor(time / 60);
    let seconds = Math.floor(time % 60);

    if (days < 10) { days = "0" + days; }
    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }

    return days + "d " + hours + "h " + minutes + "m " + seconds + "s";
}