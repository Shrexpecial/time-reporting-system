// GET ALL TASKS

window.onload = () => {
    rootUrl = window.location.protocol + '//' + window.location.host + "/Time-Reporting-System-1.0";

    getData(rootUrl + "/user/current")
        .then(result => {
            document.querySelector("#username").innerHTML = result.username;
        }).catch(error => console.error(error));

    loadTasks();
};

// LOG TIME
let logBtn = document.querySelector('#logBtn');
logBtn.addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelector("#errors").innerHTML = "";

    let taskId = document.querySelector('#task').value;
    taskId = taskId == "" ? 0 : taskId;

    let days = document.querySelector('#days').value;
    days = days == "" ? 0 : days;

    let hours = document.querySelector('#hours').value;
    hours = hours == "" ? 0 : hours;

    let mins = document.querySelector('#mins').value;
    mins = mins == "" ? 0 : mins;

    let secs = document.querySelector('#secs').value;
    secs = secs == "" ? 0 : secs;

    if (isValidLog(taskId, days, hours, mins, secs)) {
        const data = { "taskId": taskId, "days": days, "hours": hours, "minutes": mins, "seconds": secs };
        postData(rootUrl + "/user", data)
            .then(result => {
                updateRow(result.id, result.timeSpent);

                $('#time-modal').modal('hide');

            }).catch(error => {
                console.error(error);
            });
    }
});

// HELPER FUNCTIONS
function loadTasks() {
    getData(rootUrl + "/user/tasks")
        .then(result => {
            result.forEach(task => {
                createRow(task);
                addOption(task);
            });
        })
        .catch(error => {
            console.error(error);
        });
}

function createRow(task) {
    let table = document.getElementById('tasks-table').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow();

    let nameCell = newRow.insertCell(0);
    nameCell.innerHTML = task.name;

    let descrCell = newRow.insertCell(1);
    descrCell.innerHTML = task.description;

    let severityCell = newRow.insertCell(2);
    severityCell.innerHTML = task.severity;

    let reporterCell = newRow.insertCell(3);
    reporterCell.innerHTML = task.reporter;

    let timeCell = newRow.insertCell(4);
    timeCell.innerHTML = secondsToTime(task.timeSpent);
    timeCell.setAttribute("id", task.id);
}

function updateRow(taskId, timeSpent) {
    let timeCell = document.getElementById(taskId);
    timeCell.innerHTML = secondsToTime(timeSpent);
}

// function deleteAllRows() {
//     let table = document.getElementById('tasks-table').getElementsByTagName('tbody')[0];
//     let rowsNumber = table.rows.length;

//     while (rowsNumber > 0) {
//         table.deleteRow(-1);
//         rowsNumber--;
//     }
// }

function addOption(task) {
    taskSelect = document.getElementById('task');
    taskSelect.options.add(new Option(task.name, task.id));
}

function isValidLog(taskId, days, hours, minutes, seconds) {
    let isValidId = validateId(taskId);
    let isValidTime = validateTime(days, hours, minutes, seconds);

    return isValidTime && isValidId;
}

function validateTime(days, hours, minutes, seconds) {
    if (days < 0 || hours < 0 || minutes < 0 || seconds < 0) {
        displayError("Time metrics cannot be negative numbers");
        return false;
    }

    if (days == 0 && hours == 0 && minutes == 0 && seconds == 0) {
        displayError("You must enter time spent on this task");
        return false;
    }

    return true;
}

function validateId(taskId) {
    if (taskId == null || taskId == "" || taskId < 0) {
        displayError("Invalid task");
        return false;
    }

    return true;
}