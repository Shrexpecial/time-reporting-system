const loginBtn = document.querySelector("#login");
loginBtn.addEventListener("click", (e) => {
    e.preventDefault();
    document.querySelector("#errors").innerHTML = "";

    const email = document.querySelector("#emailUsername").value;
    const password = document.querySelector("#password").value;
    let data;

    if (isValidEmail(email)) {
        data = { "email": email, "username": "", "password": password };
    }
    else {
        const username = document.querySelector("#emailUsername").value;
        data = { "email": "", "username": username, "password": password };
    }

    postData(rootUrl + "/login", data)
        .then(result => {
            console.log(result);
            window.location.replace(rootUrl + "/index");
        }).catch(error => {
            console.error(error);
            displayError(error.message);
        });
});

