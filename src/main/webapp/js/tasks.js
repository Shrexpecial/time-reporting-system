let currentRow;
let currentTaskId;

// LOAD TASKS
window.onload = () => {
    rootUrl = window.location.protocol + '//' + window.location.host + "/Time-Reporting-System-1.0";

    getData(rootUrl + "/user/current")
        .then(result => {
            document.querySelector("#username").innerHTML = result.username;
        }).catch(error => console.error(error));

    loadTasks();
};

function loadTasks() {
    getData(rootUrl + "/admin/tasks")
        .then(result => {
            result.forEach(task => {
                createRow(task);
            });
        })
        .catch(error => {
            console.error(error);
        });
}

function createRow(task) {
    let table = document.getElementById('tasks-table').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow();

    let idCell = newRow.insertCell(0);
    idCell.innerHTML = task.id;

    let nameCell = newRow.insertCell(1);
    nameCell.innerHTML = task.name;

    let descrCell = newRow.insertCell(2);
    descrCell.innerHTML = task.description;

    let severityCell = newRow.insertCell(3);
    severityCell.innerHTML = task.severity;

    let reporterCell = newRow.insertCell(4);
    reporterCell.innerHTML = task.reporter;

    let timeCell = newRow.insertCell(5);
    timeCell.innerHTML = secondsToTime(task.timeSpent);

    let actionsCell = newRow.insertCell(6);
    let btnTemplate = document.querySelector('#buttons');
    let buttons = btnTemplate.content.cloneNode(true);
    actionsCell.appendChild(buttons);
    feather.replace();
}

$('#table-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal 
    var modal = $(this);
    var title = button.data('title');
    modal.find('.modal-title').text(title);
    var saveButton = $('#save-btn');

    // handle add new task
    if (title === "Add Task") {
        // Clear
        $('input[name=task-name]').val("");
        $('input[name=description]').val("");
        $('select[name=severity]').val("Medium");

        saveButton.attr('data-action', 'add');
    }
    // handle edit task
    else if (title === "Edit Task") {
        var parent = button.parent().parent();
        currentRow = parent;

        var tdId = parent.children("td:nth-child(1)").text();
        currentTaskId = tdId;

        var tdName = parent.children("td:nth-child(2)").text();
        var tdDescription = parent.children("td:nth-child(3)").text();
        var tdSeverity = parent.children("td:nth-child(4)").text();

        $('input[name=task-name]').val(tdName);
        $('input[name=description]').val(tdDescription);
        $('select[name=severity]').val(tdSeverity);

        saveButton.attr('data-action', 'edit');
    }
});

// SAVE BUTTON
let saveBtn = document.querySelector('#save-btn');
saveBtn.addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelector("#errors").innerHTML = "";

    if (e.target.getAttribute('data-action') === 'add') {
        let name = document.querySelector('#task-name').value;
        let description = document.querySelector('#description').value;
        let severity = document.querySelector('#severity').value;

        if (isValidData(name)) {
            let data = { "name": name, "description": description, "severity": severity }
            postData(rootUrl + "/admin", data)
                .then(result => {
                    console.log(result);
                    createRow(result);

                    $('#table-modal').modal('hide');

                }).catch(error => {
                    console.error(error);
                });
        }

    }
    else if (e.target.getAttribute('data-action') === 'edit') {
        let id = currentTaskId;
        let name = document.querySelector('#task-name').value;
        let description = document.querySelector('#description').value;
        let severity = document.querySelector('#severity').value;

        if (isValidData(name)) {
            let data = { "id": id, "name": name, "description": description, "severity": severity }
            postData(rootUrl + "/admin/update", data)
                .then(result => {
                    console.log(result);
                    updateTaskRow(result);

                    $('#table-modal').modal('hide');

                }).catch(error => {
                    console.error(error);
                });
        }
    }
});

function isValidData(name) {
    let isValidName = validate(name, "Task name");
    return isValidName;
}

function validate(data, string) {
    if (data == null || data == "") {
        displayError(string + " is required");
        return false;
    }
    return true;
}

function updateTaskRow(task) {
    currentRow.children("td:nth-child(2)").text(task.name);
    currentRow.children("td:nth-child(3)").text(task.description);
    currentRow.children("td:nth-child(4)").text(task.severity);
}

// DELETE TASK
$('#delete-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var parent = button.parent().parent();
    currentRow = parent;

    var tdId = parent.children("td:nth-child(1)").text();
    currentTaskId = tdId;
});

// DELETE BUTTON 
let deleteBtn = document.querySelector('#delete-btn');
deleteBtn.addEventListener('click', (e) => {
    e.preventDefault();
    console.log(currentTaskId);

    deleteData(rootUrl + "/admin/task/" + currentTaskId)
        .then(result => {
            console.log(result);
            currentRow.remove();
            $('#delete-modal').modal('hide');

        }).catch(error => {
            console.error(error);
        });

});

// VIEW ASSIGN
$('#view-modal').on('show.bs.modal', function (event) {
    deleteAllRows();
    deleteOptions();

    var button = $(event.relatedTarget);
    var parent = button.parent().parent();
    var tdId = parent.children("td:nth-child(1)").text();
    currentTaskId = tdId;

    getData(rootUrl + "/admin/users/task/" + tdId)
        .then(result => {
            console.log(result);
            result.forEach(user => createUserInTable(user));
        }).catch(error => {
            console.error(error);
        });

    getData(rootUrl + "/admin/users/not/task/" + tdId)
        .then(result => {
            console.log(result);
            result.forEach(user => addOption(user));
        }).catch(error => {
            console.error(error);
        });

});

let assignBtn = document.querySelector('#assignBtn');
assignBtn.addEventListener('click', (e) => {
    e.preventDefault();

    let usersID = $('#users').val();
    if (usersID == null || usersID.length == 0) {
        alert("No users selected");
    }
    else {
        let data = { "usersID": usersID, "taskId": currentTaskId };
        postData(rootUrl + "/admin/assignment", data)
            .then(result => {
                console.log(result);
                $('#view-modal').modal('hide');
                alert("You successfully assigned user/s to task");

            }).catch(error => {
                console.error(error);
            });
    }

});

function addOption(user) {
    userSelect = document.getElementById('users');
    userSelect.options.add(new Option(user.username, user.id));
}

function deleteOptions() {
    $("#users").empty();
}

function createUserInTable(user) {
    let table = document.getElementById('users-table').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow();

    let nameCell = newRow.insertCell(0);
    nameCell.innerHTML = user.username;

    let fullNameCell = newRow.insertCell(1);
    fullNameCell.innerHTML = user.fullName;
}

function deleteAllRows() {
    let table = document.getElementById('users-table').getElementsByTagName('tbody')[0];
    let rowsNumber = table.rows.length;

    while (rowsNumber > 0) {
        table.deleteRow(-1);
        rowsNumber--;
    }
}