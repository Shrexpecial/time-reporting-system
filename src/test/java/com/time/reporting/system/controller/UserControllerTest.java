/*
package com.time.reporting.system.controller;

import com.time.reporting.system.model.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.Calendar;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith (Arquillian.class)
public class UserControllerTest {
    @Deployment
    public static JavaArchive createDeployment () {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(UserController.class)
                .addClass(AdminController.class)
                .addClass(Role.class)
                .addClass(User.class)
                .addClass(Task.class)
                .addClass(Severity.class)
                .addClass(Log.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("META-INF/test-persistence.xml", "persistence.xml");
    }

    @Inject
    private UserController userController;
    @Inject
    private AdminController adminController;

    @Test (expected = Exception.class)
    public void createInvalidUser () {
        User user = new User(null, "12345", "dyako georgiev", "dyako@gmail.com", Role.Admin);
        userController.createUser(user);
    }

    @Test
    public void create () {
        //Create a user
        User user = new User("John", "123987", "John Green", "john@gmai.com", Role.User);
        user = userController.createUser(user);
        Long userId = user.getId();

        //Check created user
        assertNotNull(userId);

        //Find created user
        User userFound = adminController.findUser(userId);

        //Check if the user is properly persisted in the database
        assertEquals("John", userFound.getUsername());
        assertTrue(userFound.getEmail().startsWith("john@"));
    }

    @Test
    public void logEntry() {
        Task task = userController.findTask(3L);

        User user = adminController.findUser(2L);

        Log entry = new Log(user, task, 200000L, Calendar.getInstance());
        userController.logTime(entry);

        assertNotNull(entry.getLog_id());
    }
}
*/
