/*
package com.time.reporting.system.controller;

import com.time.reporting.system.controller.AdminController;
import com.time.reporting.system.controller.UserController;
import com.time.reporting.system.model.*;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.io.output.StringBuilderWriter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith (Arquillian.class)
public class AdminControllerTest {
    @Deployment
    public static JavaArchive createDeployment () {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(AdminController.class)
                .addClass(UserController.class)
                .addClass(Role.class)
                .addClass(User.class)
                .addClass(Task.class)
                .addClass(Severity.class)
                .addClass(Log.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsManifestResource("META-INF/test-persistence.xml", "persistence.xml");
    }

    @Inject
    private AdminController adminController;


    @Test (expected = Exception.class)
    public void createInvalidTask(){
        Task task = new Task(null,"Install linux with dual boot", Severity.High,"Dyako");
        adminController.createTask(task);
    }

    @Test
    public void createTask () {
        //Create a task
        Task task = new Task("go buy coca-cola","buy cola with fanta",Severity.High,"Dyako");
        task = adminController.createTask(task);
        Long taskId = task.getId();

        //Check created task
        assertNotNull(taskId);


        //Find created task
        Task taskFound = adminController.findTask(taskId);

        //Check the found user
        assertEquals("go buy coca-cola",task.getName());
        assertTrue(taskFound.getReporter().startsWith("Dyako"));
    }

    @Test
    public void updateTask(){
        //Find created task
        Task taskFound = adminController.findTask(1L);
        taskFound.setModified(Calendar.getInstance());
        adminController.updateTask(taskFound);

    }

    @Test
    public void assignNewTask(){
//    Task task = new Task();
//    task.setName("Assign a task");
//    task.setDescription("Testing purpose ");
//    task.setReporter("Lazar");
//    adminController.createTask(task);
//
//    Long id = 1L;
//    User user = this.adminController.findUser(id);
//    adminController.addTaskToUser(id, task);
//    Set<Task> tasks = user.getTasks();
//    assertNotNull(tasks);
    }

    @Test
    public void assignExistingTask(){
        Task task = adminController.findTask(3L);
        Long id = 2L;
        User user = this.adminController.findUser(id);
        adminController.addTaskToUser(id, task);
        Set<Task> tasks = user.getTasks();
        assertNotNull(tasks);
    }

    @Test
    public void removeTask(){
        adminController.deleteTask(2L);
    }

    @Test
    public void makeQuery(){

    }

    @Test
    public void getFullReport(){

    }

    @Test
    public void getSeverityReport(){

    }

}
*/
