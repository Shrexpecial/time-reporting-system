CREATE TABLE user (
  user_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  password varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  full_name varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  email varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  role enum('Admin','User') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'User',
  PRIMARY KEY (user_id)
);

CREATE TABLE task (
  task_id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  description text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  severity enum('Urgent','High','Medium','Low') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Medium',
  time_spent int(10) unsigned NOT NULL DEFAULT 0,
  modified datetime NOT NULL,
  created datetime NOT NULL,
  reporter varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (task_id)
);

CREATE TABLE users_tasks (
  user_id int(11) NOT NULL,
  task_id int(11) NOT NULL,
  PRIMARY KEY (user_id,`task_id`),
  KEY task_id (task_id),
  CONSTRAINT user_task_ibfk_1 FOREIGN KEY (user_id) REFERENCES user (user_id),
  CONSTRAINT user_task_ibfk_2 FOREIGN KEY (task_id) REFERENCES task (task_id)
);

CREATE TABLE log (
  log_id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) DEFAULT NULL,
  task_id int(11) DEFAULT NULL,
  time_spent int(10) unsigned NOT NULL DEFAULT 0,
  date datetime DEFAULT NULL,
  PRIMARY KEY (log_id),
  FOREIGN KEY (user_id) REFERENCES user (user_id),
  FOREIGN KEY (task_id) REFERENCES task (task_id)
);

INSERT INTO user (username, password, full_name, email, role) 
VALUES ("root", '1000:cdf0bf3af81113742758cb599631cac2:12199ff8bbd7bed2ac2cd214a18a397bb77534bf81824bf392d012b98cff2eac0d03a1ef180ee09269a4bdcd807311a22e4a92882e44481fc947d46e8a571dae', "Root", "root@gmail.com", "Admin");