**Time Reporting and Project Management System**
***
**Authors:** *Dyako Georgiev Dyakov, Lazar Andreev Prisov, Nicole Svetozarova Emanuilova*
***
## Used technologies 
**Front-end**
* HTML, CSS, JavaScript, Bootstrap

**Back-end**
* Java, MySQL
# Resources
* [Wilfdfly 17.0.0.Final](https://wildfly.org/)
* [MySQL](https://www.mysql.com/)
* [Arquillian](http://arquillian.org/)
* [Hibernate](https://hibernate.org/)
* [JavaMail API](https://www.oracle.com/technetwork/java/javamail/index.html)
* [iText](https://itextpdf.com/en)

## Getting Started

**Steps for installing and configuring WildFly**
* Install Java
* Download Wildfly 17.0.0 and go to **wildfly-17.0.0.Final\bin and run standalone.ps1** in PowerShell (Windows 10)
* Go to localhost:8080 and you should see WildFly welcome screen.
* Next, go back to **\wildfly-17.0.0.Final\bin** and then **Run add-user.ps1** with PowerShell (Windows). Then
add a management user by pressing enter.
* Enter the details of your user. When asked “What groups do you want this user to belong to?” press Enter and then type yes to add the user for the realm ManagemntRealm.
* When asked; “Is this new user going to be used for one AS process to connect to another AS process?” type no
* Return to the PowerShell window that has WildFly running and press ctrl+c to kill the process, the PowerShell window should close.
*  Open a bowser and navigate to  http://localhost:9990
*  You will be promoted to log in, enter the username and password that you set and click ok.
* [ Link for installation guide with pictures](https://darrenoneill.eu/?p=594)

**Deploying MySQL Connector to WildFly**
* When the server is running, go to http://localhost:9090/console
* Then go to **Deployments > and click on Add**
* Upload MYSQL driver jar file.
* Name: mysql-connector 
* Runtime Name: mysql-connector-java-8.0.16.jar 
* Then go to **Configurations > Subsystems > Datasources & Drivers > Datasources > Click on Add**
* JNDI name: java:/time-reporting
* Driver Name.: mysql-connector-java-8.0.16.jar 
* Connection URL:  jdbc:mysql://localhost/time_reporting

**Configuring Database**
* Create 'time_reporting' database.
* Run **createDatabase.sql** script to create tables and initial user with Admin privileges.

**Configuring Mail Service**
* Unavailable due to security reasons.

**Initial root user credentials:**
*  Username: root
* Password: 123456
		
## Functionality
* Administrators can create a task with the following properties: name, description, assignee, severity. Upon creation the task reporter becomes the currently logged admin.
* Users can report time they spent on a different task. The time can be in the following format: - Seconds - Minutes - Hours - Day (1d = 8h)
* Administrators can assign users to a particular task.
* The task reporter receives notification emails for every activity related to the task.
* When a given task is assigned to a particular user, the user receives email notification.
* Administrators can generate reports.
